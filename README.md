WAR

This project is a Spring Hibernate web app implementation of the simple card game War. Playable online here:
http://ec2-54-191-27-210.us-west-2.compute.amazonaws.com:8080/war/

Game is playable by one person. The player has 3 options: draw, automatic, or go back. Draw is a button that will play one turn of the card game. Automatic will run all turns and resolve the game immediately. Go back is to quit. 

Design Doc (Google Drive):
https://bit.ly/2HRfYKp
